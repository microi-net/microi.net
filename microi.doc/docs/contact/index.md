# 联系我们

如果你正在使用这个项目或者需要商务合作，可以扫码联系我们！

|商务(邓总)|商务(唐总)|商务(刘老师)|技术(李赛赛)|个人版(老周)|
|:--:|:--:|:--:|:--:|:--:|
| <img src="/contact/dengzong.jpeg" data-fancybox="gallery" alt="Wechat QRcode" width=200> | <img src="/contact/tangzong.jpeg" data-fancybox="gallery" alt="Wechat QRcode" width=200> | <img src="/contact/shangwu.jpg" data-fancybox="gallery" alt="Wechat QRcode" width=200> | <img src="/contact/jishu.jpg" data-fancybox="gallery" alt="Alipay QRcode" width=200> | <img src="/contact/anderson.jpeg" data-fancybox="gallery" alt="Alipay QRcode" width=200> |

>* 购买【个人版】需告知您的推荐来源（了解Microi吾码的渠道）
>* 购买【企业版】请联系【商务】

## 吾码服务商
>* 宁波小吾科技有限公司
>* 宁波思值科技发展有限公司
>* 宁波予珩数字科技有限公司
>* 宁波崇石科技有限公司
>* 宁波威讯软件有限公司
>* 宁波微众信息科技有限公司
>* 成都道斯科技有限公司
>* 成都汇编科技有限公司
>* 浙江集马科技有限公司
>* 四川微吾科技有限公司
>* 湖北小吾科技有限公司
---
layout: home

title: Microi吾码
titleTemplate: Open Source Low-Code Platform

hero:
  name: Micro<span style="-webkit-text-fill-color:red !important;">i</span>吾码
  text: Open Source Low-Code Platform
  tagline: .NET9 + Redis + MySql/SqlServer/Oracle + Vue2/3 + Element-UI/Element-Plus + UniApp X
  actions:
    - theme: brand
      text: Documentation
      link: /doc/index
    - theme: brand
      text: WebOS Trial
      link: https://webos.microi.net
    - theme: alt
      text: Traditional Interface Trial
      link: https://demo.microi.net
    - theme: alt
      text: Gitee Source Code
      link: https://gitee.com/ITdos/microi.net
  image:
    src: /home2.jpg
features:
  
  - icon: ⚒
    title: Open Source Engines
    details: Form Engine, Interface Engine, AI Engine, UI Engine, Print Engine, Workflow Engine, Office Engine, Module Engine, Template Engine, Collection Engine, Scheduling Engine, Data Source Engine, SaaS Engine, Search Engine, Message Queue Engine, etc.
  - icon: 🐋
    title: Distributed Architecture
    details: Supports distributed deployment, Docker, K8S, Jenkins, Rancher, CICD, RabbitMQ, Redis Sentinel, ES Search Engine, MongoDB, Distributed Storage OSS/MinIO/Amazon S3, etc.
  - icon: 🆕
    title: Cross-Platform, Cross-Database, Cross-Language
    details: Supports Linux, Windows, domestic operating systems, Alibaba Cloud/Tencent Cloud/local deployment; supports MySql5.5+, SqlServer2000+, Oracle11g+ databases; supports database sharding, read-write separation, multi-master synchronization; provides gRPC source code, supports secondary development in .Net, Java, Python, PHP, etc.
  - icon: 💽
    title: No Restrictions
    details: No restrictions on the number of users, forms, data volume, number of databases, etc., provides front-end and back-end, microservice framework source code, supports secondary development with Vue, ReactJS, AngularJs
  - icon: 🧩
    title: Black Technology
    details: Custom components, application marketplace, multi-database expansion, microservices, task scheduling system, custom export templates, single sign-on, chat system, public account platform management, etc.
  - icon: 📱
    title: Developer-Oriented
    details: Ordinary users can configure simple applications online; complex business logic can be implemented through the JavaScript V8 engine
---
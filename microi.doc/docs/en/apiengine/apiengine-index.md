# 接口引擎实战
>* 发送第三方短信：[https://microi.blog.csdn.net/article/details/143990546](https://microi.blog.csdn.net/article/details/143990546)
>* 发送阿里云短信：[https://microi.blog.csdn.net/article/details/143990603](https://microi.blog.csdn.net/article/details/143990603)
>* 自定义导出Excel：[https://microi.blog.csdn.net/article/details/143619083](https://microi.blog.csdn.net/article/details/143619083)
>* 微信小程序授权手机号登录：[https://microi.blog.csdn.net/article/details/144106817](https://microi.blog.csdn.net/article/details/144106817)
>* 微信v3支付JSAPI下单：[https://microi.blog.csdn.net/article/details/144156119](https://microi.blog.csdn.net/article/details/144156119)
>* 微信支付回调接口：[https://microi.blog.csdn.net/article/details/144168810](https://microi.blog.csdn.net/article/details/144168810)
>* MongoDB相关操作：[https://microi.blog.csdn.net/article/details/144434527](https://microi.blog.csdn.net/article/details/144434527)

## 更多平台内置接口引擎详见
[https://demo.microi.net/#/api-engine](https://demo.microi.net/#/api-engine)
# 基于Microi吾码的成功案例-服装生产ERP【目前已售97套】
## 服装生产ERP【目前已售97套】
>* 开发周期1个月，开发人员华经理、小金，由客户研发团队开发，Microi吾码官方团队提供技术支持
## 服装ERP简介
>* 截止2024-11-08，该ERP项目已配置146个菜单模块、141张物理表，68个接口引擎，总数据量已超过1000万
>* 截止2024-11-08，该ERP项目已开80个SaaS数据库，即已应用到80家服装工厂
>* 该产品仍在销售递推中，**截止2024-12-02【目前已售97套】**
## 技术亮点
>* 该产品实现了动态尺码数据【行转列】，动态控制列的显隐
>* 通过接口引擎实现逻辑非常复杂的裁床分包、裁床配比、分包明细、菲票明细生成算法
## 项目截图
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/ce6e4df87965495293b1a6eacb428a63.png#pic_center)

![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/188a498311e34b85b7952600fdb0c296.png#pic_center)
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/6a0f745c1533496a8a45b0ae32478c26.png#pic_center)
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/ffab33b507cc42b2915ca8e4df58a6b5.png#pic_center)
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/bb0cb5421768457aabd1f6aec62f02e4.png#pic_center)
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/9f15fe14d6144c6a9898447f8b156f25.png#pic_center)
## 已开80个SaaS数据库，即80家服装厂
![已开80个SaaS数据库，即80家服装厂](https://static.itdos.com/upload/img/csdn/5bf61ae3a9c24f99919bbab22415a192.png#pic_center)

## 项目在线demo试用
* 商业项目，若对此产品感兴趣，请联系博主
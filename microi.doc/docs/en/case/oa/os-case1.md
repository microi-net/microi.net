# 基于Microi吾码的成功案例-集团、国企OA办公系统+ERP

## 在线试用地址
>* 地址：[https://nbcmc-dev.microi.net](https://nbcmc-dev.microi.net)
>* 帐号：admin
>* 密码：1234567

## 集团、国企OA办公系统+ERP
* 国企产品，集团架构
* 开发周期2个月，由客户1人开发，Microi吾码官方团队提供技术支持
## 集团、国企OA办公系统+轻量级ERP系统简介、亮点
* 已配置150+功能模块、170张物理表，28个接口引擎、49个流程引擎
* 配套小程序流程审批、多考勤点打卡
## 系统截图
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/8d3b87fbe84f4ddc9a834cf7503975c5.png#pic_center)
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/bce5e80892d244608e78a9d5db4462b1.png#pic_center)
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/b3f86e979bf8419782fc31831d2b69ae.png#pic_center)
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/2e58c1b198ad435cb2e8fc1c14f53b93.png#pic_center)
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/df02a5dc69284a0c9c59a769232d6ee1.png#pic_center)

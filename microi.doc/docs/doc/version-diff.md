# 开源版、个人版、企业版区别
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/1c35b220183e42428981f0a474f93e96.png#pic_center)

## 版本区别
>* **开源版**：平台传统界面前端100%完整源码、平台后端90%以上源代码
>* **个人版**：￥999；额外包含【vue3 WebOS操作系统界面100%完整源码】等，功能与企业版无任何差别
>* **企业版**：￥10w（首付￥2w）；额外包含【移动端uniapp uni-ui 100%完整源码、打印引擎源码、界面引擎源码】；提供更多的培训、咨询等售后服务；优先响应平台升级需求

## 区别地址
>见：[https://microi.net/microi-price](https://microi.net/microi-price)


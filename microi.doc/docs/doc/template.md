# CSDN文章模板
## 开源低代码平台-Microi吾码-平台简介
>* 技术框架：.NET8 + Redis + MySql/SqlServer/Oracle + Vue2/3 + Element-UI/Element-Plus
>* 平台始于2014年（基于Avalon.js），2018年使用Vue重构，于2024年10月29日开源
>* Vue3试用地址（仅查询）：[https://microi.net](https://microi.net)
>* Vue2传统界面试用地址（可操作数据）：[https://demo.microi.net/](https://demo.microi.net/)
>* Gitee开源地址：[https://gitee.com/ITdos/microi.net](https://gitee.com/ITdos/microi.net)
>* GitCode开源地址：[https://gitcode.com/microi-net/microi.net/overview](https://gitcode.com/microi-net/microi.net/overview)
>* 官方CSDN博客：[https://microi.blog.csdn.net](https://microi.blog.csdn.net/?type=blog)

## 平台预览图
<img src="https://static.itdos.com/upload/img/csdn/ee76765ec943d4da0b6f6097c494d8bc.jpeg" style="margin: 5px;">

```js
这里插入文章正文
```

## Microi吾码 - 系列文档
>* **平台介绍**：[https://microi.blog.csdn.net/article/details/143414349](https://microi.blog.csdn.net/article/details/143414349)
>* **一键安装使用**：[https://microi.blog.csdn.net/article/details/143832680](https://microi.blog.csdn.net/article/details/143832680)
>* **快速开始使用**：[https://microi.blog.csdn.net/article/details/143607068](https://microi.blog.csdn.net/article/details/143607068)
>* **源码本地运行-后端**：[https://microi.blog.csdn.net/article/details/143567676](https://microi.blog.csdn.net/article/details/143567676)
>* **源码本地运行-前端**：[https://microi.blog.csdn.net/article/details/143581687](https://microi.blog.csdn.net/article/details/143581687)
>* **Docker部署**：[https://microi.blog.csdn.net/article/details/143576299](https://microi.blog.csdn.net/article/details/143576299)
>* **表单引擎**：[https://microi.blog.csdn.net/article/details/143671179](https://microi.blog.csdn.net/article/details/143671179)
>* **模块引擎**：[https://microi.blog.csdn.net/article/details/143775484](https://microi.blog.csdn.net/article/details/143775484)
>* **接口引擎**：[https://microi.blog.csdn.net/article/details/143968454](https://microi.blog.csdn.net/article/details/143968454)
>* **工作流引擎**：[https://microi.blog.csdn.net/article/details/143742635](https://microi.blog.csdn.net/article/details/143742635)
>* **界面引擎**：[https://microi.blog.csdn.net/article/details/143972924](https://microi.blog.csdn.net/article/details/143972924)
>* **打印引擎**：[https://microi.blog.csdn.net/article/details/143973593](https://microi.blog.csdn.net/article/details/143973593)
>* **V8函数列表-前端**：[https://microi.blog.csdn.net/article/details/143623205](https://microi.blog.csdn.net/article/details/143623205)
>* **V8函数列表-后端**：[https://microi.blog.csdn.net/article/details/143623433](https://microi.blog.csdn.net/article/details/143623433)
>* **V8.FormEngine用法**：[https://microi.blog.csdn.net/article/details/143623519](https://microi.blog.csdn.net/article/details/143623519)
>* **Where条件用法**：[https://microi.blog.csdn.net/article/details/143582519](https://microi.blog.csdn.net/article/details/143582519)
>* **DosResult说明**：[https://microi.blog.csdn.net/article/details/143870540](https://microi.blog.csdn.net/article/details/143870540)

>* **分布式存储配置**：[https://microi.blog.csdn.net/article/details/143763937](https://microi.blog.csdn.net/article/details/143763937)
>* **自定义导出Excel**：[https://microi.blog.csdn.net/article/details/143619083](https://microi.blog.csdn.net/article/details/143619083)
>* **表单引擎-定制组件**：[https://microi.blog.csdn.net/article/details/143939702](https://microi.blog.csdn.net/article/details/143939702)
>* **表单控件数据源绑定配置**：[https://microi.blog.csdn.net/article/details/143767223](https://microi.blog.csdn.net/article/details/143767223)
>* **复制表单和模块到其它数据库**：[https://microi.blog.csdn.net/article/details/143950112](https://microi.blog.csdn.net/article/details/143950112)
>* **论传统定制开发与低代码开发的优缺点**：[https://microi.blog.csdn.net/article/details/143866006](https://microi.blog.csdn.net/article/details/143866006)
>* **开源版、个人版、企业版区别**：[https://microi.blog.csdn.net/article/details/143974752](https://microi.blog.csdn.net/article/details/143974752)
>* **成为合伙人**：[https://microi.blog.csdn.net/article/details/143974715](https://microi.blog.csdn.net/article/details/143974715)

>* **基于Microi的开源项目**：[https://microi.blog.csdn.net/category_12828230.html](https://microi.blog.csdn.net/category_12828230.html)
>* **成功案例**：[https://microi.blog.csdn.net/category_12828272.html](https://microi.blog.csdn.net/category_12828272.html)

>* **接口引擎实战-发送第三方短信**：[https://microi.blog.csdn.net/article/details/143990546](https://microi.blog.csdn.net/article/details/143990546)
>* **接口引擎实战-发送阿里云短信**：[https://microi.blog.csdn.net/article/details/143990603](https://microi.blog.csdn.net/article/details/143990603)
>* **接口引擎实战-微信小程序授权手机号登录**：[https://microi.blog.csdn.net/article/details/144106817](https://microi.blog.csdn.net/article/details/144106817)
>* **接口引擎实战-微信v3支付JSAPI下单**：[https://microi.blog.csdn.net/article/details/144156119](https://microi.blog.csdn.net/article/details/144156119)
>* **接口引擎实战-微信支付回调接口**：[https://microi.blog.csdn.net/article/details/144168810](https://microi.blog.csdn.net/article/details/144168810)
>* **接口引擎实战-MongoDB相关操作**：[https://microi.blog.csdn.net/article/details/144434527](https://microi.blog.csdn.net/article/details/144434527)
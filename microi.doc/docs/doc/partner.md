
# 成为合伙人
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/e7ad67f4d9d7421c9dcf6fb49d637e18.png#pic_center)

## 开源合作
> 为促进开源项目发展，博文合作推广请与博主CSDN私信沟通

## 合作申请地址
>* **合作申请地址**：[https://microi.net/partner](https://microi.net/partner)
>* 开源合作模块低代码平台实现代码：[https://microi.blog.csdn.net/article/details/143995349](https://microi.blog.csdn.net/article/details/143995349)
## 申请条件
<!-- （满足任一条件） -->
>* **购买过个人版**的用户
<!-- >* **未购买个人版**但在某个领域有一定的影响力（如CSDN博主超过1000粉丝、企业高管等） -->


## 吾码开源版、个人版、企业版区别
>开源版、个人版（￥999）、企业版（￥10w/年）区别：
>[https://microi.blog.csdn.net/article/details/143974752](https://microi.blog.csdn.net/article/details/143974752)

## 吾码团队、博主简介
>**有大佬合作前在咨询目前吾码目前相关情况，这里做统一简介：**

>* **2008年**：经历四川汶川大地震后，博主离开四川来到沿海城市宁波打工（全栈工程师，.net + php + java + asp + jquery + photoshop + flash + 服务器等）
>* **2014年**：博主从前端MVC架构（backbone.js、jquery等）转型**MVVM**（Avalon.js），使用UEditor设计表单，开发**自定义平台**（当时没意识到**低代码**这个词）
>* **2015年**：[成都道斯科技有限公司](https://www.tianyancha.com/company/3223822415)成立，官网[https://iTdos.com](https://www.itdos.com)，开源了[Dos.ORM](https://www.oschina.net/p/dos-orm?hmsr=aladdin1e1)，当时QQ交流群3000余大佬加入，现在优秀的ORM太多，不再推荐Dos.ORM（更新慢），Microi吾码默认使用Dos.ORM，**支持切换ORM**！
>* **2018年**：**iTdos.DIY**低代码平台前端采用**Vue**进行重构，后端采用【.NET Core2】重构
>* **2019年**：[宁波小吾科技有限公司](https://www.tianyancha.com/company/3360828707)成立，**iTdos.DIY**低代码平台更名为**Microi吾码**
>* **2020年**：**小吾科技获【￥1000万】天使轮风投**，此时研发团队30余人
>* **2018~2023年期间**：Microi吾码**企业版**已应用到**5家上市公司研发团队**、多家国企、**数百家**企业，基于吾码的应用软件目前百余套，demo持续整理更新中！
>* **2024年11月**：Microi吾码**正式开源**，并开放**个人版￥999**，功能上与企业版无任何区别
## 公司相关照片
![公司前台](https://static.itdos.com/upload/img/csdn/7b68764268b84a559521f2c13c9b49ed.jpeg#pic_center)
![公司团队](https://static.itdos.com/upload/img/csdn/a7973e787f16416c90c4e398f57f3a5e.jpeg#pic_center)
![公司阳台](https://static.itdos.com/upload/img/csdn/da2afeb3d25346b58a1e36c0813181e5.jpeg#pic_center)
![公司融资海报](https://static.itdos.com/upload/img/csdn/57a04e9b5dc34cf1a4b2ef00682511b4.png#pic_center)
![随拍Microi吾码奖项](https://static.itdos.com/upload/img/csdn/6a90558b44fe4c75a74f12cc77992508.jpeg#pic_center)
![随拍Microi吾码限定茶](https://static.itdos.com/upload/img/csdn/6b6c0ca20a3b4fe1acdbf9d846c94229.jpeg#pic_center)


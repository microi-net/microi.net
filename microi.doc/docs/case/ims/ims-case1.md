# 基于Microi吾码的成功案例-集团制造协同系统
## 集团制造协同系统
* A股上市公司产品
* 开发周期3个月+，由客户研发团队10余人开发，Microi吾码官方团队提供技术支持
## 集团制造协同系统简介、亮点
* 已配置200+功能模块、241张物理表，339个接口引擎
* 对接飞书，在飞书上处理逻辑流程、消息通知
* Microi吾码平台扩展集团内部11个第三方系统数据库
* 多数据库、多表统计虚拟报表，并实现虚拟报表的增、删、改业务逻辑
## 系统截图
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/15e6381172324784b1c6ffb1fb8b1c65.png#pic_center)
## 200余张表
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/8dd357af4d9e47c0bdb967cf012fbc6f.png#pic_center)
## 300余个接口引擎
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/e5b3169f3e844d7bbd63866158d3e9b9.png#pic_center)



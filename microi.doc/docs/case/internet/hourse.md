# 基于Microi吾码的成功案例-互联网房地产二手房、新房、租房平台
## 互联网房地产二手房、新房、租房平台
* 开发周期3个月，开发人员5人以上，由Microi吾码官方团队开发
## 互联网房地产简介、亮点
* 高峰时400+线下门店、2000+房屋经纪人、总部100+人
* 配置100余个菜单模块、128张物理表
* 大量的前端微服务定制开发、后端二开，表单引擎嵌入vue定制组件、定制组件二次调用表单引擎
* 对接了阿里云隐私号（经纪人绑定隐私号进行电话营销），平台记录通话语音内容
* 对接了VR设备
* 完整的PC端官网、平台管理系统、手机端ios app、安卓app、微信小程序、h5
* 实现了地图找房、地铁找房、画圈找房
## 产品截图
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/22e4c2c07c26414daf97ca71e22765dd.png#pic_center)
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/6e6fb6199dd0467ebbc62c9fc1bee790.png#pic_center)
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/8f85afeabd9d4b8f904c501da800ecc3.png#pic_center)
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/ac83c155ccc44726907eb90577514ffc.jpeg#pic_center)
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/dac6239ca3394770800bdd0f4e3f8dca.png#pic_center)

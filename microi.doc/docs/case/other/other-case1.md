# 简介
## 迈巴赫物性
>* 由Microi吾码官方团队开发，持续维护升级
## 亮点
>* 已配置50+功能模块、70+张物理表，65+接口引擎
>* 配套小程序、APP
>* 通过接口引擎实现复杂的物性算法
## 相关截图
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/192518027ee84c50b2a6760018e9a85b.jpeg#pic_left)![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/7ef16904b5274a0ebbcc16beefd1d863.jpeg#pic_left)![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/cc82e8729ae74699960c7901c2e80019.jpeg#pic_left)
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/c7c67d8a17a04cec834cba07ffba14a0.png#pic_center)

![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/6b8e9029caa842b8b7a6cf8dc1359bf3.png#pic_center)

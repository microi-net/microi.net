# ## 基于Microi吾码的成功案例-标准CRM产品
## 标准CRM产品
* 开发周期1个月，由客户研发团队+吾码官方团队共同开发
## 亮点
* 已配置200+功能模块、252张物理表，76个接口引擎、29个工作流程引擎
* 配套低代码平台标准H5移动端（PC端配置后移动端同步生效）
## 系统截图
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/174326525ae54cd1a943e403422f6104.png#pic_center)
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/a67a8e8ff56a49dd9170cd8daa4fa9cc.png#pic_center)
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/62cc7966da2d4791924a633596b9ee8f.png#pic_center)
![在这里插入图片描述](https://static.itdos.com/upload/img/csdn/2dc41f6ac2824e4694840ff67dd0cb30.jpeg#pic_center)

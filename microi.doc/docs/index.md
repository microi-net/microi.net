---
layout: home

title: Microi吾码
titleTemplate: 开源低代码平台

hero:
  name: Micro<span style="-webkit-text-fill-color:red !important;">i</span>吾码
  text: 开源低代码平台
  tagline: .NET9 + Redis + MySql/SqlServer/Oracle + Vue2/3 + Element-UI/Element-Plus + UniApp X
  actions:
    - theme: brand
      text: 文档
      link: /doc/index
    - theme: brand
      text: WebOS试用
      link: https://webos.microi.net
    - theme: alt
      text: 传统界面试用
      link: https://demo.microi.net
    - theme: alt
      text: Gitee源码
      link: https://gitee.com/ITdos/microi.net
  image:
    src: /home2.jpg
features:
  
  - icon: ⚒
    title: 开源引擎
    details: 表单引擎、接口引擎、AI引擎、界面引擎、打印引擎、工作流引擎、Office引擎、模块引擎、模板引擎、采集引擎、调度引擎、数据源引擎、SaaS引擎、搜索引擎、消息队列引擎等
  - icon: 🐋
    title: 分布式架构
    details: 支持分布式部署，支持Docker、K8S、Jenkins、Rancher、CICD、RabbitMQ、Redis哨兵、ES搜索引擎、MongoDB、分布式存储OSS/MinIO/亚马逊S3等
  - icon: 🆕
    title: 跨平台、跨数据库、跨语言
    details: 支持Linux、Windows、国产等操作系统，支持阿里云/腾讯云/本地化部署；支持MySql5.5+、SqlServer2000+、Oracle11g+等数据库；支持分库分表、读写分离、多主同步等；提供gRPC源码，支持.Net、Java、Python、PHP等二次开发
  - icon: 💽
    title: 无限制
    details: 不限制用户数、表单数、数据量、数据库数量等，提供前后端、微服务框架源代码，支持Vue、ReactJS、AngularJs进行二次开发
  - icon: 🧩
    title: 黑科技
    details: 定制组件、应用商城、多数据库扩展、微服务、任务调度系统、自定义导出模板、单点登陆、聊天系统、公众号平台管理等
  - icon: 📱
    title: 面向开发者
    details: 普通用户可在线配置简单应用；而复杂业务逻辑均能通过JavaScript V8引擎实现
---
